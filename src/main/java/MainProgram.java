public class MainProgram {
    public static void main(String[] args) {
        MainProgram app = new MainProgram();
        app.run2();
    }

    void run2() {
        int[] a = {1, 2, 0, 3, 5, 6, 0};
        int i = findElement(a, 6);
    }

    public int findElement(int[] arr, int val) {
        if (arr == null) {
            return -1;
        }

        for (int i = 0; i < arr.length; ++i) {
            if (arr[i] < 1) {
                continue;
            }
            if (arr[i] == val) {
                return i;
            }
        }
        return -1;
    }

    void run3() {
        int[] a = {1, 2, 0, 3, 5, 6, 0};
        moveNonZeroToBeginning(a);
        System.out.println("test " + a);
    }

    // abcdbdgasdgsdagswetwegsgasdgsadgasdgsadg
    public String reverseString_right(String param) {
        if (param == null) {
            return null;
        }

        StringBuilder res = new StringBuilder();

        for (int i = 0; i < param.length(); ++i) {
            char[] p = {param.charAt(param.length() - 1 - i)};
            res.append(new String(p));
        }

        return res.toString();
    }

    public String reverseString_wrong(String param) {
        if (param == null) {
            return null;
        }

        String res = new String();

        for (int i = 0; i < param.length(); ++i) {
            char[] p = {param.charAt(param.length() - 1 - i)};
            res = res + new String(p);
        }

        return res;
    }

    // https://gitlab.com/inzanej58/tsatsokho-stc2219-repository/-/merge_requests/5/diffs#1c48d0e784c9cd17c7bbf0b7ad51b32b0b79756a
    void moveNonZeroToBeginning(int[] array) {
        int nonZeroIndex = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] != 0) {
                if (i != nonZeroIndex) {
                    array[nonZeroIndex] = array[i];
                    array[i] = 0;
                }
                nonZeroIndex++;
            }
        }
    }

    int[] moveNonZeroToBeginning_2(int[] array) {
        int[] res = new int[array.length];
        int j = 0;

        for (int i = 0; i < array.length; ++i) {
            if (array[i] != 0) {
                res[j] = array[i];
                ++j;
            }
        }

        return res;
    }
}
