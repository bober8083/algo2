import org.junit.jupiter.api.Test;

import java.util.Random;

class MainTestTest {
    private static final Random RAND = new Random();

    @Test
    public void test_1() {
        MainProgram t = new MainProgram();
        int[] arr = {1, 2, 0, (RAND.nextInt() % 7), 5, 6, 0};
        int arg = 6;
        assert t.findElement(arr, arg) == 5;
    }

    @Test
    public void test_2() {
        MainProgram t = new MainProgram();
        int[] arr = {1, 2, 0, 3, 5, 6, 0};
        int arg = 7;
        assert t.findElement(arr, arg) == -1;
    }

    @Test
    public void test_3() {
        MainProgram t = new MainProgram();
        int[] arr = null;
        int arg = 5;
        assert t.findElement(arr, arg) == -1;
    }

    @Test
    public void test_rev_0() {
        MainProgram t = new MainProgram();
        assert null == t.reverseString_wrong(null);
    }

    @Test
    public void test_rev_1() {
        MainProgram t = new MainProgram();
        String a = "a";
        String ans = t.reverseString_wrong(a);
        assert "a".equals(ans);
    }

    @Test
    public void test_rev_2() {
        MainProgram t = new MainProgram();
        String a = "ab";
        assert "ba".equals(t.reverseString_wrong(a));
    }

    @Test
    public void test_rev_3() {
        MainProgram t = new MainProgram();
        String a = "abagsgsdgsdgsadgsadgsadgsdagsadgsdagasdgsdagsdagsadgsadgsadg";
        assert "ba".equals(t.reverseString_wrong(a));
    }
}