import org.junit.jupiter.api.Test;

class MainProgramTest {
    @Test
    public void we_want_to_test_it() {
        MainProgram t = new MainProgram();
        int[] arr = null;
        int arg = 5;
        assert t.findElement(arr, arg) == -1;
    }

    @Test
    public void test_1() {
        MainProgram t = new MainProgram();
        int[] p = {0, 2, 3};
        int[] pOut = {2, 3, 0};
        int[] r = t.moveNonZeroToBeginning_2(p);
        assert equalArrays(pOut, r);
    }

    @Test
    public void test_2() {
        MainProgram t = new MainProgram();
        int[] p = {0, 0, 3};
        int[] pOut = {3, 0, 0};
        int[] r = t.moveNonZeroToBeginning_2(p);
        assert equalArrays(pOut, r);
    }

    static boolean equalArrays(int[] a, int[] b) {
        if (a.length != b.length) {
            return false;
        }
        for (int i = 0; i < a.length; ++i) {
            if (a[i] != b[i]) {
                return false;
            }
        }
        return true;
    }
}